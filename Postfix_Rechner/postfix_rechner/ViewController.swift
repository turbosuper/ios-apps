//
//  ViewController.swift
//  postfix_rechner
//
//

import UIKit

class ViewController: UIViewController {
 
    
    @IBOutlet weak var LabelOperand1: UILabel!
    @IBOutlet weak var LabelOperand2: UILabel!
    @IBOutlet weak var ErgebnisLabel: UILabel!
    @IBOutlet weak var OperatorLabel: UILabel!
    @IBOutlet weak var Last10Display: UILabel!
    
    @IBOutlet weak var Vorzeichen: UILabel!
    
    var Operand1 = false
    var number1 :Int = 0
    var number2 :Int = 0
    var ergebnis :Int = 0
    var operator1 :String = ""
    var Operations = [String]()
    
    func createOperand(digit: String) -> Int{
        Vorzeichen.text = ""
        OperatorLabel.text  = ""
        if LabelOperand1.text     == "0"{
            LabelOperand1.text = ""
        }
        LabelOperand1.text?.append(digit)
        return Int(LabelOperand1.text!)!
    }
    
    func createOperand2(digit: String) -> Int{
        OperatorLabel.text = ""
        Vorzeichen.text = ""
        if LabelOperand2.text     == "0"{
            LabelOperand2.text = ""
        }
        LabelOperand2.text?.append(digit)
        return Int(LabelOperand2.text!)!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        LabelOperand1.text = String(number1)
        LabelOperand2.text = String(number2)
        OperatorLabel.text = operator1
        ErgebnisLabel.text = String(ergebnis)
        Last10Display.text = ""
        Vorzeichen.text = ""
       }


    @IBAction func pressed(_ sender: UIButton) {
       if !Operand1 {

        let pressed_num:String = sender.currentTitle!
            number1 = createOperand(digit: pressed_num)
        }
        else {
            let pressed_num:String = sender.currentTitle!
            number2 = createOperand2(digit: pressed_num)
        }
        
    }
    
    @IBAction func OKbutton(_ sender: UIButton) {
        Operand1 = !Operand1
    }
    
    @IBAction func OperatorFunc(_ sender: UIButton) {
        operator1 = sender.currentTitle!
        OperatorLabel.text = operator1
        if ( number1 != nil && number2 != nil && operator1 != nil){
            switch operator1{
            case "+":
                ergebnis = number1 + number2
            case "-":
                ergebnis = number1 - number2
            case "*":
                ergebnis = number1 * number2
            case "/":
                if number2 == 0 {
                    print("Divide by zero")
                } else {
                ergebnis = number1 / number2
                }
            default:
                print ("All ready")
            
            }
        }
        
        if (ergebnis < 0 ){
            Vorzeichen.text = "N"
        }
        else {
            Vorzeichen.text = "P"
        }
        SaveResult(calc_operator: operator1)
        OperatorLabel.text = String(ergebnis)
        number1 = ergebnis
        number2 = 0
        Operand1 = false
        ErgebnisLabel.text = String(ergebnis)
        LabelOperand1.text = String(number1)
        LabelOperand2.text = String(number2)
        OperatorLabel.text = operator1
    }
    
    @IBAction func clear(_ sender: UIButton) {
        ergebnis = 0
        number1 = 0
        number2 = 0
        Operand1 = false
        operator1 = ""
        ErgebnisLabel.text = String(ergebnis)
        LabelOperand1.text = String(number1)
        LabelOperand2.text = String(number2)
        OperatorLabel.text = operator1
    }
    

    func SaveResult(calc_operator: String){
        if (Operations.count == 10){
            Operations.removeFirst()
        }
        
        Operations.append(calc_operator)
        Last10Display.text = ""
        for (i, txt) in Operations.enumerated(){
            Last10Display.text?.append("\(i+1):\(txt); ")
          //  if i < 9 {
            //    Last10Display.text?.append("\n")
              //  }
            }
    }
}


