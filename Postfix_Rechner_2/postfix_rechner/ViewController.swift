//
//  ViewController.swift
//  postfix_rechner
//
//

import UIKit

class ViewController: UIViewController {
 
    
    @IBOutlet weak var LabelOperand1: UILabel!
 
    
    var Operand1 = false
    var number1 :Int = 0
    var number2 :Int = 0
    var ergebnis :Int = 0
    var operator1 :String = ""
    
    
    func createOperand(digit: String) -> Int{
        if LabelOperand1.text     == "0"{
            LabelOperand1.text = ""
       }
       
        LabelOperand1.text?.append(digit)
        return Int(LabelOperand1.text!)!
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        LabelOperand1.text = String(number1)
  
       }


    @IBAction func pressed(_ sender: UIButton) {
       if !Operand1 {

        let pressed_num:String = sender.currentTitle!
            number1 = createOperand(digit: pressed_num)
        }
        else {
            let pressed_num:String = sender.currentTitle!
            number2 = createOperand(digit: pressed_num)
        }
        
    }
    
    @IBAction func OKbutton(_ sender: UIButton) {
        LabelOperand1.text = ""
        Operand1 = !Operand1
    }
    
    @IBAction func OperatorFunc(_ sender: UIButton) {
        operator1 = sender.currentTitle!
        //OperatorLabel.text = operator1
        if ( number1 != nil && number2 != nil && operator1 != nil){
            switch operator1{
            case "+":
                ergebnis = number1 + number2
            case "-":
                ergebnis = number1 - number2
            case "*":
                ergebnis = number1 * number2
            case "/":
                if number2 == 0 {
                    print("Divide by zero")
                } else {
                ergebnis = number1 / number2
                }
            default:
                print ("All ready")
            
            }
        }
        LabelOperand1.text = String(ergebnis)
        number1 = ergebnis
        number2 = 0
        Operand1 = false
        operator1 = ""

    }
    
    @IBAction func clear(_ sender: UIButton) {
        ergebnis = 0
        number1 = 0
        number2 = 0
        Operand1 = false
        operator1 = ""
        LabelOperand1.text = String(number1)

    }
    
}


