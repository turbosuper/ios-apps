//
//  TTTbrain.swift
//  Tic_Tac_Toe
//
//

import UIKit

class TTTbrain: NSObject {
    
    var goalX = 0
    var goalO = 0

    var Xsequencefound = false
    var Osequencefound = false
    
    var newStartNecessary = true
    
    var checkedFields = ["a": "", "b": "",
                         "c": "", "d": "", "e": "", "f": "",
                         "g": "", "h": "", "i": ""]
    
    var fieldLabelDictionary = ["a": 101, "b": 102,
                         "c": 103, "d": 104, "e": 105, "f": 106,
                         "g": 107, "h": 108, "i": 109]
    
    var winningSequence = ["abc", "adg", "beh", "cfi", "def", "ghi", "aei", "ceg"]
    
    var bindedGUI : ViewController?
    
    var checkedX = ""
    
    var checkedO = ""
    
func isGameOver(_ isXnow : Bool){
        
        //generate Strings based on the checked boxes
        for (keys, values) in checkedFields{
            if (values == "O") {
                checkedO = checkedO + keys}
            else if (values == "X") {
                checkedX = checkedX + keys}
            }
        
        //step over each winning Sequence, evaluate for X
        for single_sequence in winningSequence {
           // print("1. sequence to compare is  for X is now: ", terminator:"")
           // print(single_sequence)
            //step over each letter of winning sequence
            for letter in single_sequence {
            //    print("2. Letter of  single winning sequence is now: ", terminator:"")
            //    print(letter)
                //checkd if it is present in checkedX
                for single_character in checkedX{
                  //  print("3. letter of checked X is now ", terminator:"")
                  //  print(single_character)
                    if (letter == single_character) {
                        //count matching letters
                        goalX += 1
                        if (goalX == 3){ //3 letters match - must be a winning sequence
                            Xsequencefound = true
                            break}
                    }
          //          print("4. goals X scored:", terminator:"")
            //        print(goalX)
                   // if sequencefound{break}
                }
                if Xsequencefound{break}
            }
            if Xsequencefound{break}
           // print("goal O reset")
            goalX = 0
        }
        //print("Xsequencefound is now :  ")
        //print(Xsequencefound)

        //step over each winning Sequence, evaluate for O
        for single_sequence in winningSequence {
            //     print("5. sequence to compare for O is now: ", terminator:"") ; print(single_sequence)
            //step overe every letter of winning sequence
            for letter in single_sequence {
                //print("6. Letter of  single winning sequence is now: ", terminator:"")
               //print(letter)
                //checkd if it is present in checkedO
                for single_character in checkedO{
                  // print("7. letter of checked O is now: ", terminator:"")
                   //print(single_character)
                    if (letter == single_character) {
                     //   print("antoher O found")
                        goalO += 1
                        if (goalO == 3){ //3 letters match - must be a winning sequence
                       //     print("Found it! GoalO is: ", terminator:"")
                         //   print(goalO)
                            Osequencefound = true
                            break}
                        break
                    }
                    //print("8. goals O scored: ", terminator:"")
                    //print(goalO)
                }
                if Osequencefound{break}
            }
            if Osequencefound{break}
            //print("goal O reset")
            goalO = 0
        }
        //print("Osequencefound is now: ")
        //print(Osequencefound)
        
        if (Osequencefound || Xsequencefound){
            newStartNecessary = true
            goalO = 0
            goalX = 0
            bindedGUI!.a.isEnabled = false
            bindedGUI!.b.isEnabled = false
            bindedGUI!.c.isEnabled = false
            bindedGUI!.d.isEnabled = false
            bindedGUI!.e.isEnabled = false
            bindedGUI!.f.isEnabled = false
            bindedGUI!.g.isEnabled = false
            bindedGUI!.h.isEnabled = false
            bindedGUI!.i.isEnabled = false
            
            if Osequencefound {
                bindedGUI!.whoWon.text = "O wins, game over"
                }
            else {
                bindedGUI!.whoWon.text = "X wins, game over"
                }
            }
        
        print("Checked O is:", terminator:"")
        print(checkedO)
        print("Checked X is:", terminator:"")
        print(checkedX)
        checkedO = ""
        checkedX = ""
        
    }
    
func AImove(){
    
        var madeAMove = false
   
        //the first two turns make random move
        if (calculateCheckedFields(Fields: checkedFields) < 3){
            makeARandomMove(checkedFields: &checkedFields)
            madeAMove = true
            }
        //for the next two turns check for a winning sequence
        else {
            var isThereASequence : Bool
            var cleverMove = ""
            print("Entered the AI. The dangerous sequense is: ", terminator: "")
            
            //try to find pattern in player's moves
            let dangerousSequence =  checkForWinnigSequence(checkedFields: checkedFields)
            print(dangerousSequence)
            
            //set the bool if a pattern in player's moves found
            if (dangerousSequence == "") {
                isThereASequence = false
                } else {
                isThereASequence = true
                }
            print("The isThereASequence bool is now: ", terminator: ""); print(isThereASequence)
            
            //when sequencce found try to move to the best place in the pattern
            if (isThereASequence){
                
                //get place to move
                cleverMove = getCleverMove(dangerousSequence : dangerousSequence)
                print("Entering the clever move sequence. The clever move to go is: ", terminator : "") ; print(cleverMove)
                
                //move to clever place, when it is empty. Otherwise move randomly
                if (isFieldEmpty(letter: cleverMove, checkedFields: checkedFields)){
                    //get the Tag for this Field
                    let tagLabelforCleverMove = getFieldTag(letter: cleverMove, fielddict: fieldLabelDictionary)
                    print("Got the tag for the label to move to! it is: ", terminator : ""); print(tagLabelforCleverMove)
                    let moveLabelforAI = bindedGUI.self?.view.viewWithTag(tagLabelforCleverMove) as! UILabel
                    moveLabelforAI.text = "X"
                    checkedFields.updateValue("X", forKey: cleverMove)
                    madeAMove = true
                    isGameOver((bindedGUI?.isXnow)!)
                } else {
                    makeARandomMove(checkedFields: &checkedFields)
                    madeAMove = true
                    isGameOver((bindedGUI?.isXnow)!)
                }
                
            }else {
                makeARandomMove(checkedFields: &checkedFields)
                madeAMove = true
                isGameOver((bindedGUI?.isXnow)!)
            }

        }
    
        //check again if mov is made
        if !madeAMove {
            makeARandomMove(checkedFields: &checkedFields)
        }
    
        isGameOver((bindedGUI?.isXnow)!)
        
        //set turn for human player
        bindedGUI?.isXnow = false
    }
    
func calculateCheckedFields(Fields : Dictionary<String, String>) -> Int{
        var i = 0
        for (key, value) in Fields {
            if (value != "") { i = i+1 }
        }
        return i
    }
    
func getRandomField(Fields: Dictionary<String, String>)-> String{
        let Fields = ["a", "b", "c", "d", "e", "f", "g", "h", "i"]
        let Position = Int(arc4random()) % 9
        return(Fields[Position])
    }
    
    
func getFieldTag(letter: String, fielddict: Dictionary< String, Int>)->Int {
        for (key, value) in fielddict {
            if (key == letter) { return value }
        }
        return -1
    
}
    
func isFieldEmpty(letter: String, checkedFields : Dictionary<String, String>) -> Bool{
        if (checkedFields[letter] == ""){return true}
        else {return false}
}
    //let AIButton = bindedGUI.self?.view.viewWithTag(666) as! UIButton
    //AIButton.setTitle("Palying with AI",for: .normal)
    
func checkForWinnigSequence(checkedFields : Dictionary<String, String>) -> String{
        var sequenceToTryfound = false
        var sequenceToTry = ""
        var foundOs  = 0
        //clear checkedO
        checkedO = ""
        
        //generate strings where O's are checked
        for (keys, values) in checkedFields{
            if (values == "O") {
                 checkedO = checkedO + keys}
            
        //find a sequence where player wins in next move
        for single_sequence in winningSequence {
            //  print("5. sequence to compare for O is now: ", terminator:"")
            //  print(single_sequence)
            
            //step over every letter of winning sequence
            for letter in single_sequence {
                //    print("6. Letter of  single winning sequence is now: ", terminator:"")
                //    print(letter)
            
                //checkd if it is present in checked0
                for single_character in checkedO{
                    //     print("7. letter of checked O is now: ", terminator:"")
                    //     print(single_character)
                    if (letter == single_character) {
                        //count matching letters
                        foundOs += 1
                        if (foundOs == 2){ //2 letters match - might be a winning sequence
                          //  print("Found it!")
                            //save the winning sequence
                            sequenceToTry  = single_sequence
                            sequenceToTryfound = true
                            break}
                    }
                }
                if sequenceToTryfound{break}
            }
            if sequenceToTryfound{break}
            foundOs = 0
            }

        }
     //   print("From AImove func: ", terminator : "")
      //  print(checkedO, terminator: "")
       // print(checkedX)
        checkedO = ""
        if sequenceToTryfound{ return sequenceToTry}
        else {return ""}
    }

func makeARandomMove(checkedFields : inout Dictionary<String, String>){
        //generate random field and check if it is empty
        var randomField = getRandomField(Fields: checkedFields)
       // print("RANDOM FIELD OUTSIDE the loop is:", terminator: "")
       // print(randomField)
      //  print("Value for a isFieldEmpty bool")
       // print((isFieldEmpty(letter: randomField, checkedFields: checkedFields)))
        
        while !(isFieldEmpty(letter: randomField, checkedFields: checkedFields)){
            randomField = getRandomField(Fields: checkedFields)
           // print("RANDOM FIELD in the loop is:", terminator: "")
           // print(randomField)
        }
        
        //get the tag corresponding to that random field Label
        let currentTagLabelforAI = getFieldTag(letter: randomField, fielddict: fieldLabelDictionary)
        
        //put X on this Label
        let currentLabelforAI = bindedGUI.self?.view.viewWithTag(currentTagLabelforAI) as! UILabel
        currentLabelforAI.text = "X"
        
        //update the dictionary
        checkedFields.updateValue("X", forKey: randomField)
        
    }
    
//function that goes through the sequence and finds  field that is in line with O's
func getCleverMove(dangerousSequence : String) -> String{
        print("The dangoures string in the getCleverMove is: ", terminator: ""); print(dangerousSequence)
        var cleverMove = ""
      
        //generate fields with O's in
        var fieldsFullwithO = ""
        for (keys, values) in checkedFields{
            if (values == "O") {
                fieldsFullwithO = fieldsFullwithO + keys}
        }
      //  print("Fields with Os in them: ", terminator: ""); print(fieldsFullwithO)
      
        //check which chhecked field is not yet in the winning sequence
        for letter in dangerousSequence{
            if (!fieldsFullwithO.contains(letter)){
                    cleverMove.append(letter)
                }
        }
        
        //print("The field that is missing (clever to move to): ", terminator : "")
        //print(cleverMove)
        return cleverMove
    }
}
