//
//  ViewController.swift
//  Tic_Tac_Toe
//
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var a: UIButton!
    @IBOutlet weak var b: UIButton!
    @IBOutlet weak var c: UIButton!
    @IBOutlet weak var d: UIButton!
    @IBOutlet weak var e: UIButton!
    @IBOutlet weak var f: UIButton!
    @IBOutlet weak var g: UIButton!
    @IBOutlet weak var h: UIButton!
    @IBOutlet weak var i: UIButton!
    
    @IBOutlet weak var aLabel: UILabel!
    @IBOutlet weak var bLabel: UILabel!
    @IBOutlet weak var cLabel: UILabel!
    @IBOutlet weak var dLabel: UILabel!
    @IBOutlet weak var eLabel: UILabel!
    @IBOutlet weak var fLabel: UILabel!
    @IBOutlet weak var gLabel: UILabel!
    @IBOutlet weak var hLabel: UILabel!
    @IBOutlet weak var iLabel: UILabel!
    
    @IBOutlet weak var whoWon: UILabel!
    @IBOutlet weak var clear: UIButton!
    
    @IBOutlet weak var AI_on_off: UIButton!
    let Brain = TTTbrain()
    
    var isXnow = true
    var isAIon = false
    
    var fieldLetter : String = ""
   // var currentLabel : String = ""
    //var labels = [aLabel, bLabel, cLabel, dLabel, eLabel, fLabel, gLabel, hLabel, iLabel]
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        Brain.bindedGUI = self
        
        a.tag = 1
        b.tag = 2
        c.tag = 3
        d.tag = 4
        e.tag = 5
        f.tag = 6
        g.tag = 7
        h.tag = 8
        i.tag = 9
        aLabel.tag = 101
        bLabel.tag = 102
        cLabel.tag = 103
        dLabel.tag = 104
        eLabel.tag = 105
        fLabel.tag = 106
        gLabel.tag = 107
        hLabel.tag = 108
        iLabel.tag = 109
        AI_on_off.tag = 666
        
        // Do any additional setup after loading the view, typically from a nib.
            aLabel.text = ""
            bLabel.text = ""
            cLabel.text = ""
            dLabel.text = ""
            eLabel.text = ""
            fLabel.text = ""
            gLabel.text = ""
            hLabel.text = ""
            iLabel.text = ""
    
        whoWon.text = "X's turn"
        
    }
    //
    @IBAction func chooseAi(_ sender: Any) {
        ResetAll()
        let AIButton = self.view.viewWithTag(666) as! UIButton
        
        if (!isAIon){
            isAIon = true
            AIButton.setTitle("Play with human", for: .normal)
            Brain.AImove()
        }
        else{
            isAIon = false
            AIButton.setTitle("Play with AI", for: .normal)
        }
    }
    
    @IBAction func FieldDisplay(_ sender: UIButton) {
        makeATurn(sender: sender)
    }


    @IBAction func onClear(_ sender: Any) {
        ResetAll()
    }
    
    //Put 'X' and 'O' on clicked fields
    func makeATurn(sender : UIButton){
        let currentLabelTag = sender.tag + 100
        
        let currrentButton = self.view.viewWithTag(sender.tag) as! UIButton
        
        let currentLabel = self.view.viewWithTag(currentLabelTag) as! UILabel
        
        if (currentLabel.text == "" ){
            
            if isXnow {
                if isAIon{
                   Brain.AImove()
                }else{
                    whoWon.text = "O's turn"
                    Brain.checkedFields.updateValue("X", forKey: sender.currentTitle!)
                    currentLabel.text = "X"
                    //dump(Brain.checkedFields)
                    Brain.isGameOver(isXnow)
                    isXnow = !isXnow
                }
            }
            else {
                if isAIon {
                    whoWon.text = "O's turn"
                }else{
                    whoWon.text = "X's turn"
                }
                    Brain.checkedFields.updateValue("O", forKey: sender.currentTitle!)
                currentLabel.text = "O"
                //dump(Brain.checkedFields)
                Brain.isGameOver(isXnow)
                isXnow = !isXnow
                if isAIon {Brain.AImove()}
            }
        }
    }
    
    func ResetAll(){
        //isAIon = false
        isXnow = true
        aLabel.text = ""
        bLabel.text = ""
        cLabel.text = ""
        dLabel.text = ""
        eLabel.text = ""
        fLabel.text = ""
        gLabel.text = ""
        hLabel.text = ""
        iLabel.text = ""
        
        a.isEnabled = true
        b.isEnabled = true
        c.isEnabled = true
        d.isEnabled = true
        e.isEnabled = true
        f.isEnabled = true
        g.isEnabled = true
        h.isEnabled = true
        i.isEnabled = true
        
        Brain.Xsequencefound = false
        Brain.Osequencefound = false
        
        for (keys, _) in Brain.checkedFields{
            Brain.checkedFields.updateValue("", forKey: keys)
        }
        
        whoWon.text = ""
        
        if isAIon {
            Brain.AImove()
        }
    
       // print(Brain.checkedFields)
    }
}

